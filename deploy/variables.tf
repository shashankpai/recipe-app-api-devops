variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "xyz@devops.com"

}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the postgres RDS instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}